### What is nvGrid? ###

* a responsive grid
* a new standard for NV Interactive projects

### How do I get set up? ###

This version of the grid is can either be cloned into your project, or it is available via Bower by running `bower install nvGridTest`.  As the name suggests, this is a test and not the live implementation planned for the project.

Once this install is complete you'll need to import the _nvGrid.scss file into your sass (`@import "./bower_components/nvGridTest/nvGrid";` or similar. 

The repository also includes `_nvGridVars.scss` which is simply an example of the variables you can use in your the sass you write. These variables should be included *before* importing nvGrid.scss. nvGrid sets some defaults for these if you don't specifically set them earlier. Supported vars include:

* **$bodyMax** - the max width of the body or wrapper element (denoted by an element with the class of `b`). Default for this is 1400px (ems are thus far untested)
* **$bodyMaxSlim** - an alternative (smaller) max width value for an element with a class of `b-s`
* **$gridCols** - the number of columns in the grid. This (theoretically) could be any number at all and defaults to 12.
* **$boxSizing** - this sets the box-sizing model to use for *all* elements on the page. It defaults to `border-box`
* **$breakpoints** - a sass list of all possible breakpoints for use in both breakpoint naming and sizing. Defaults to `("sm": 548px, "md": 768px, "lg": 1024px, "xl": 1280px);`
* **$gutterValue** - an arbitrary number used to set column gutters. The default is 28 which is largely meaningless, but computes the gutters to 2.333% for 12 columns and 1.166% for 24 columns for example (because this just looks nice). This should probably be reviewed to make more sense.

### How do I use nvGrid? ###
Usage is similar to PureCSS or the  yui grid (see [purecss.io](http://purecss.io) for more information on the former).  We've reduced the need for really long class names though, replacing Pure's complex `pure-u-size-num1-num2` format with `gi-size-num`, where size is a customisable breakpoint name (see above) and num is a number of columns wide out of the total number of columns (also customisable as per above). So, using the package defaults (12 columns):

* **gi-12** - use all 12 columns (full available width) at standard screen width - equivalent of pure's pure-u-1-1
* **gi-sm-6** - use 6 columns (1/2 of available width) - equivalent of pure's pure-u-sm-1-2
* **gi-xl-7** - use 7 columns (7/12 of available width) - equivalent of pure's pure-u-xl-7-12

The theory is that while purecss aims to provide easy to understand fractions, sometime these can be complex to work with, so stripping everything back to *x* columns out of *y* simplifies readability and writing of css/sass.

There are a number of other helper classes to managed padding and margins, etc.  Note: Padding and margin classes should *not* be used together (room for development here?)

* **gi-p** - uses the padding set by $gutterValue (above) on both left and right sides of the grid item. This (and the other gi-* rules) inherit from .gi, so will *replace* the gi class on the grid item. 
* **gi-pr / gi-pl** - apply padding to only the right or only the left of the grid item
* **gi-m** - uses margin on both left and right of grid item (instead of padding)
* **gi-mr / gi-ml** - apply margin to only the right or only the left of the grid item
* **g-vam** - apply vertical-align middle to a grid item 
 
There are also some helper classes for the non-grid items so they fit nicely with the look and feel of the grid
* **ng-p** - equal padding on left and right
* **ng-pr / ng-pl** - apply padding to only the right or only the left of the element
* **ng-m** - uses margin on both left and right of element (instead of padding)
* **ng-mr / gi-ml** - apply margin to only the right or only the left of the element

### Contribution guidelines ###

Always happy to take suggestions and updates to the code we've written. Any pull requests should be well tested and well documented.

### Who do I talk to? ###

This repo is owned by NV Interactive. Contact bren.murrell@nvinteractive.com or diego.sieg@nvinteractive.com for further info.